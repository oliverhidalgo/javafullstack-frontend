import { TOKEN_AUTH_USERNAME } from './../_shared/var.constants';
import { Perfil } from 'src/app/_model/perfil';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TOKEN_NAME } from '../_shared/var.constants';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PerfilService {
  perfil = new Perfil();

  constructor() { 
  }

  listar(){
    const helper = new JwtHelperService();

    sessionStorage.getItem(TOKEN_NAME);

    let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodedToken = helper.decodeToken(tk.access_token);

    console.log(decodedToken.user_name);
    this.perfil.nombreUsuario = decodedToken.user_name;
    this.perfil.roles = decodedToken.authorities;
   
    return this.perfil;
  }
  
  
}
