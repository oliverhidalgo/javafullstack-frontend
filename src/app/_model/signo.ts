import { Paciente } from 'src/app/_model/paciente';

export class Signo {
    idSigno: number;
    paciente: Paciente;
    fecha: string;
    temperatura: string;
    pulso: string;
    ritmo: string;
}