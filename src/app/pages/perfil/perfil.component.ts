import { Component, OnInit } from '@angular/core';
import { TOKEN_NAME } from 'src/app/_shared/var.constants';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Perfil } from 'src/app/_model/perfil';
import { PerfilService } from 'src/app/_service/perfil.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  public perfilUs = new Perfil();


  constructor(private perfilService: PerfilService) { }

  ngOnInit() {
    this.listar();
  }

  listar(){
   this.perfilUs = this.perfilService.listar();
  }

}
