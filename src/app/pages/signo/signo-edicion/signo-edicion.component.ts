import { ActivatedRoute, Router, Params } from '@angular/router';
import { SignoService } from '../../../_service/signo.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Signo } from '../../../_model/signo';
import { Component, OnInit } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  form: FormGroup;
  myControlPaciente: FormControl = new FormControl();

  id: number;
  signo: Signo;
  edicion: boolean = false;
  pacientes: Paciente[] = [];
  pacienteSeleccionado: Paciente;
  idPacienteSeleccionado: number;
  fechaSeleccionada: Date;
  maxFecha: Date = new Date();
  filteredOptions: Observable<any[]>;

  constructor(private pacienteService: PacienteService, private signoService: SignoService, private route: ActivatedRoute, private router: Router) {
    this.signo = new Signo();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(''),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl(''),
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];

      this.edicion = params['id'] != null;
      this.initForm();
    });

    this.listarPacientes();
    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  initForm() {
    if (this.edicion) {
      this.signoService.listarSignoPorId(this.id).subscribe(data => {
        let id = data.idSigno;
        let paciente = data.paciente;
        let fecha = data.fecha;
        let temperatura = data.temperatura;
        let pulso = data.pulso;
        let ritmo = data.ritmo;        

        this.form = new FormGroup({
          'id': new FormControl(id),
          'paciente': new FormControl(paciente),
          'fecha': new FormControl(fecha),
          'temperatura': new FormControl(temperatura),
          'pulso': new FormControl(pulso),
          'ritmo': new FormControl(ritmo)
        });

        this.idPacienteSeleccionado = paciente.idPaciente;

      });
    }
  }

  operar() {
    let paciente = new Paciente();
    paciente.idPaciente = this.idPacienteSeleccionado;

    this.signo.idSigno = this.form.value['id'];
    this.signo.paciente = this.form.value['paciente'];
    this.signo.fecha = moment(this.fechaSeleccionada).toISOString();
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmo = this.form.value['ritmo'];

    if (this.signo != null && this.signo.idSigno > 0) {
      this.signoService.modificar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(especialidad => {
          this.signoService.signosCambio.next(especialidad);
          this.signoService.mensajeCambio.next("Se modifico");
        });
      });
    } else {
      this.signoService.registrar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(especialidad => {
          this.signoService.signosCambio.next(especialidad);
          this.signoService.mensajeCambio.next("Se registro");
        });
      });
    }

    this.router.navigate(['signo']);
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }
}
