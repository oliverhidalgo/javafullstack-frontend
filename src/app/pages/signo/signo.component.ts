import { SignoService } from '../../_service/signo.service';
import { Signo } from '../../_model/signo';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Paciente } from 'src/app/_model/paciente';

@Component({
  selector: 'app-signo',
  templateUrl: './signo.component.html',
  styleUrls: ['./signo.component.css']
})
export class SignoComponent implements OnInit {
 
  displayedColumns = ['id', 'nombres', 'apellidos', 'fecha',  'temperatura', 'pulso', 'ritmo', 'acciones'];
  dataSource: MatTableDataSource<Signo>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  mensaje: string;

  constructor(private signoService: SignoService, public snackBar: MatSnackBar) {  }

  ngOnInit() {

    this.signoService.signosCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    
    this.signoService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.signoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000,
      });
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idSigno: number) {
    this.signoService.eliminar(idSigno).subscribe(data => {
      this.signoService.listar().subscribe(data => {
        this.signoService.signosCambio.next(data);
        this.signoService.mensajeCambio.next('Se eliminó');
      });
    });
  }


}
